package com.thalesgroup.genericconnector.ZMQ;

import com.thalesgroup.genericconnector.Application;
import org.slf4j.LoggerFactory;
import org.zeromq.ZMQ;

public class ZMQPUSHPULLHelper {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Application.class);

    public static void startSink() {
        //  Prepare our context and socket
        ZMQ.Context context = ZMQ.context(1);
        ZMQ.Socket receiver = context.socket(ZMQ.PULL);
        receiver.connect("tcp://*:5558");
        logger.debug("Connected to port: 5558");
        //  Wait for start of batch
        String string = new String(receiver.recv(0));

        //  Start our clock now
        long tstart = System.currentTimeMillis();

        while (!Thread.currentThread ().isInterrupted ()) {
            string = new String(receiver.recv(0)).trim();
            logger.debug("received message: " + string);
        }
        //  Calculate and report duration of batch
        long tend = System.currentTimeMillis();

//        System.out.println("\nTotal elapsed time: " + (tend - tstart) + " msec");
        receiver.close();
        context.term();

    }

    public static void startPull(int port) {
        ZMQ.Context context = ZMQ.context(1);
        ZMQ.Socket receiver = context.socket(ZMQ.PULL);
        receiver.bind("tcp://*:" + String.valueOf(port));
        logger.debug("Connected to port: " + port);
        //  Wait for start of batch
        byte[] bytes = receiver.recv();
        bytes.toString();
        //  Start our clock now
        long tstart = System.currentTimeMillis();

        while (!Thread.currentThread ().isInterrupted ()) {
            String string = new String(receiver.recv(0)).trim();
            logger.debug("received message: " + string);
        }
        //  Calculate and report duration of batch
        long tend = System.currentTimeMillis();

//        System.out.println("\nTotal elapsed time: " + (tend - tstart) + " msec");
        receiver.close();
        context.term();
    }

    public static void startPull1(int port) {

    }
}
