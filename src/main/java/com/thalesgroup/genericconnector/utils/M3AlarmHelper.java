package com.thalesgroup.genericconnector.utils;

import com.thalesgroup.genericconnector.alertStructure.Alert;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
//import imageProcessing.Images;
import net.iharder.Base64;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class M3AlarmHelper {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(M3AlarmHelper.class);

	/*private Govtech govtech;

	public Govtech getGovtechInstance() {
		if (govtech == null) {
			govtech = new Govtech();
		}
		return govtech;
	}

	public static Govtech getGovTech() {
		return new M3AlarmHelper().getGovtechInstance();
	}*/

	public static String getDate() {

		Date date = new Date();
		SimpleDateFormat dt = new SimpleDateFormat("yyyy/MM/dd");
		String formattedDate = dt.format(date);

		return formattedDate;

	}

	public static String getTransId(String message) {
		String string = message;
		String[] parts = string.split(":");
		String transId = parts[2];
		return transId;
	}

	public static void updateLatestImage(String userToken, String imageID) throws IOException {

		String token = userToken;
		String hurl = "http://192.168.19.11/filesrv/imgs?id=" + imageID;
		logger.info("Image Url: " + hurl);
		URL url = new URL(hurl);

		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.setRequestProperty("UserToken", token);

		InputStream is = request.getInputStream();
		ByteArrayOutputStream os = new ByteArrayOutputStream();

		byte[] b = new byte[2048];
		int length;

		while ((length = is.read(b)) != -1) {
			os.write(b, 0, length);
		}

		is.close();
		os.close();

		byte[] response = os.toByteArray();
		FileOutputStream fos = new FileOutputStream(
				"C:\\Users\\t0100802\\Desktop\\Hugo\\Eclipse\\3MConnection\\src\\LatestImage.jpg");
		fos.write(response);
		fos.close();

	}

	public static String createImageString() throws IOException {

		String encodedFile = Base64.encodeFromFile(
				"C:\\Users\\t0100802\\Desktop\\Hugo\\Eclipse\\3MConnection\\src\\LatestResizedImage.jpg");

		return encodedFile;

	}

	public static void updateAlarmData(String transID) throws IOException, ParserConfigurationException,
			TransformerException, JAXBException, InterruptedException {

		String token = AuthentificationHelper3M.getToken();

		String datenow = M3AlarmHelper.getDate();
		String hurl = ConfigurationHelper.getConfigHelperInstance().get3MServerIP() + "/historysrv/records/latest/" + datenow + "?count=1";
		URL url = new URL(hurl);

		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.setRequestProperty("UserToken", token);

		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		JsonArray rootarray = root.getAsJsonArray();
		logger.info("JSON array" + rootarray);

		Object[] jsonTable = Reader.getJsonData(rootarray);
		String level = (String) jsonTable[16];

		if (level.equals("HIT")) {

			logger.info("Known person detected");
			String imageID = (String) jsonTable[1];
			//ToDo:Check what to do with new image
			M3AlarmHelper.updateLatestImage(token, imageID);
			logger.info("Image downloaded");
//			Images.resizeImage();
//			logger.info("Image resGovtechized");
			Alert alert = GovtechAlarmHelper.createAlert(jsonTable);
			GovtechAlarmHelper.createXMLFile(alert);
			logger.info("Latest XML created");
			GovtechAlarmHelper.sendNewXML();
			logger.info("Alert sent to HMI");
			logger.info("Current Time: " + new Date().getTime());
			logger.info("");
		}

		else {
			logger.info("Unknown person, no alarm sent");
			logger.info("");
		}
	}

}
