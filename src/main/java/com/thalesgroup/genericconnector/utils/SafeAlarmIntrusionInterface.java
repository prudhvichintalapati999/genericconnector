package com.thalesgroup.genericconnector.utils;

public interface SafeAlarmIntrusionInterface {
	public static final String STREAM_ID = "streamId";
	public static final String DURATION = "duration";
	public static final String SDK_ID = "sdkId";
	public static final String SDKCONF_ID = "sdkConfId";
	public static final String STATUS = "status";
	public static final String EVENTID = "eventId";
	public static final String IMGPATH = "imgPath";
	public static final String VIDEOPATH = "videoPath";
	public static final String SCENENAME = "sceneName";
	public static final String NAME = "name";
	public static final String CONTEXT_NAME = "contextName";
	public static final String GENTIMESTAMP = "genTimestampMs";
	public static final String TIMESTAMP = "timestamp";
	public static final String LOCATION = "location";
	
	/*
	 * Check config in Safe to check the configured streamid
	 * Eg: "Airport_Gate_F52"
	 */
	public void setStreamId(String streamID);
	/*
	 * Eg: 0
	 */
	public void setDuration(int duration); 
	/*
	 * Eg: "Evitech-Jaguar"
	 */
	public void setSdkId(String sdkId);
	/*
	 * Eg: "1487811291000Alarm122"
	 */
	public void setSdkConfId(String sdkConfId);
	/*
	 * Eg: "ACTIVE"
	 */
	public void setStatus(String status);
	/*
	 * Eg: "1487811291000Alarm122"
	 */
	public void setEventId(String eventId);
	/*
	 * the configured path is /safe/media in safe
	 * Eg: "/defaultScene_2017-08-01_16-33-26_Condition_1.jpg"
	 */
	public void setImgPath(String imgPath);
	/*
	 * Eg: "/safe/v0/media/videos/jaguar/20170104/jaguar-intrusion2-1483525435116.mp4"
	 */
	public void setVideoPath(String videoPath);
	/*
	 * Eg: "defaultScene"
	 */
	public void setSceneName(String sceneName);
	/*
	 * Eg: "door_intrusion"
	 */
	public void setName(String name);
	/*
	 * Eg: "D�faut"
	 */
	public void setContextName(String contextName);
	/*
	 * Eg: "1501641187000"
	 */
	public void setGenTimeStamp(long genTimeStamp);
	/*
	 * Eg: "1501641187000"
	 */
	public void setTimeStamp(long timeStamp);
	/*
	 * Eg: "1.3432334,103.6563"
	 * Eg: "" If nothing is specified takes the cameras location
	 */
	public void setLocation(String location);
}
