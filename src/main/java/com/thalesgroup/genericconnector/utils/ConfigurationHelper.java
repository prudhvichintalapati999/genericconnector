package com.thalesgroup.genericconnector.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ConfigurationHelper {

    private static ConfigurationHelper configHelper;

    @Value("${pullport:0}")
    private String PULLPORT;

    @Value("${jag_reception_port:0}")
    private String JAG_RECPTION_PORT;

    @Value("${3m_websocket_uri:configuration}")
    private String M3_WEBSOCKET_URI;

    @Value("${3m_authentification_uri}")
    private String M3_AUTHENTIFICATIONURI;

    @Value("${3m_requestproperty_pwd}")
    private String M3_REQUESTPROPERTY_PWD;

    @Value("{3m_server_ip}")
    private String M3_SERVER_IP;

    @Value("{3m_registration_id}")
    private String M3_REGISTRATION;

    @Value("{3m_message_index}")
    private String M3_MESSAGEINDEX;

    public int getPullPort() {
        return Integer.parseInt(PULLPORT);
    }

    public int getJagReceptionPort() {
        return Integer.parseInt(JAG_RECPTION_PORT);
    }

    public String get3MWebSocketUri() {
       return M3_WEBSOCKET_URI;
    }

    public String get3MAuthentificationUri() {
        return M3_AUTHENTIFICATIONURI;
    }

    public String get3MRequestPropertyPwd() {
        return M3_REQUESTPROPERTY_PWD;
    }

    public String get3MServerIP() {
        return M3_SERVER_IP;
    }

    public String get3MRegistrationId() {
        return M3_REGISTRATION;
    }

    public String get3MMessageIndex() {
        return M3_MESSAGEINDEX;
    }

    public static void setConfigurationInstance(ConfigurationHelper configurationHelper) {
        configHelper = configurationHelper;
    }

    public static ConfigurationHelper getConfigHelperInstance() {
        return configHelper;
    }


}
