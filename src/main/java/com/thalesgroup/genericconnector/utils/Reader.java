package com.thalesgroup.genericconnector.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;

public class Reader {

    public static void saveImage(String imageUrl, String destinationFile) throws IOException {
        URL url = new URL(imageUrl);
        InputStream is = url.openStream();
        OutputStream os = new FileOutputStream(destinationFile);

        byte[] b = new byte[2048];
        int length;

        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }

        is.close();
        os.close();
    }

    public static JsonArray readJsonFromFile(String fileName)
            throws FileNotFoundException, IOException, ParseException {

        JsonParser parser = new JsonParser();
        Object obj = parser.parse(new FileReader(fileName));

        JsonArray jsonArray = (JsonArray) obj;
        System.out.println(jsonArray);

        return jsonArray;
    }

    public static JsonArray readArrayFromUrl(String historysrv) throws IOException {

        URL url = new URL(historysrv);

        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.connect();

        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        JsonArray rootarray = root.getAsJsonArray();

        System.out.println(rootarray);
        return rootarray;

    }

    public static JsonObject readObjectFromUrl(String surl) throws IOException {

        URL url = new URL(surl);

        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.connect();

        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        JsonObject rootobject = root.getAsJsonObject();

        System.out.println(rootobject);
        return rootobject;

    }


    public static Object[] getJsonData(JsonArray jsonarray) throws IOException {


        JsonArray jsonArray = jsonarray;

        Object[] jsonTable;
        jsonTable = new Object[22];


        for (Object o : jsonArray) {

            JsonObject alarm = (JsonObject) o;

            String personId = alarm.get("personId").getAsString();
            //System.out.println(personId);
            jsonTable[0] = personId;

            String coverImageId = alarm.get("coverImageId").getAsString();
            //System.out.println(coverImageId);
            jsonTable[1] = coverImageId;

            String imageCount = alarm.get("imageCount").getAsString();
            //System.out.println(imageCount);
            jsonTable[2] = imageCount;

            String firstAppearedTime = alarm.get("firstAppearedTime").getAsString();
            jsonTable[3] = firstAppearedTime;

            String lastAppearedTime = alarm.get("lastAppearedTime").getAsString();
            jsonTable[4] = lastAppearedTime;

            String sourceId = alarm.get("sourceId").getAsString();
            jsonTable[5] = sourceId;

            String sourceName = alarm.get("sourceName").getAsString();
            jsonTable[6] = sourceName;

            String sourceLocation = alarm.get("sourceLocation").getAsString();
            jsonTable[7] = sourceLocation;

            String enrolledImageCount = alarm.get("enrolledImageCount").getAsString();
            jsonTable[8] = enrolledImageCount;

            String enrolledSuccessCount = alarm.get("enrolledSuccessCount").getAsString();
            jsonTable[9] = enrolledSuccessCount;

            String enrollStatus = alarm.get("enrollStatus").getAsString();
            jsonTable[10] = enrollStatus;

            String createTime = alarm.get("createTime").getAsString();
            jsonTable[11] = createTime;

            String modifyTime = alarm.get("modifyTime").getAsString();
            jsonTable[12] = modifyTime;

            JsonObject hitInfo = alarm.get("hitInfo").getAsJsonObject();

            String personIdInfo = hitInfo.get("personId").getAsString();
            jsonTable[13] = personIdInfo;

            String personName = hitInfo.get("personName").getAsString();
            //System.out.println(personName);
            jsonTable[14] = personName;

            String sceneId = hitInfo.get("sceneId").getAsString();
            jsonTable[15] = sceneId;

            String level = hitInfo.get("level").getAsString();
            jsonTable[16] = level;

            String category = hitInfo.get("category").getAsString();
            jsonTable[17] = category;

            Float score = hitInfo.get("score").getAsFloat();
            jsonTable[18] = score;

            String watchListId = hitInfo.get("watchListId").getAsString();
            jsonTable[19] = watchListId;

            String videoId = alarm.get("personId").getAsString();
            jsonTable[20] = videoId;

            String alert = alarm.get("alert").getAsString();
            jsonTable[21] = alert;


        }

        return jsonTable;
    }


}


