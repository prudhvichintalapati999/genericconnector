package com.thalesgroup.genericconnector.utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
    private static void extractResponse(String response) throws ParseException {
        // TODO Auto-generated method stub
        String[] split = response.split("@");

        // Get type of alarm, which can only be written manually in Jaguar in
        // the name of the alarm
        String[] nameAlarm = split[4].split("_");
        String typeAlarm = nameAlarm[0];
        System.out.println("Type of alarm: " + typeAlarm);

        IntrusionAlarm jagIntrusionAlarm = new IntrusionAlarm();
        jagIntrusionAlarm.setStreamId("camera" + split[7]);
        jagIntrusionAlarm.setDuration(0);
        jagIntrusionAlarm.setSdkId("Evitech-Jaguar");
        jagIntrusionAlarm.setStatus("ACTIVE");

        String eventId = split[2];
        jagIntrusionAlarm.setEventId(eventId);

        // Image path not received currently. Done manually in next section
        // String splitFile[] = split[3].split("/");
        // alarm.addProperty("imgPath", "/" + splitFile[splitFile.length - 1]);

        final String OLD_FORMAT_PATH = "yyyy-MM-dd_hh:mm:ss";
        final String NEW_FORMAT_PATH = "yyyy-MM-dd_HH-mm-ss";
        String oldDateStringPath = split[3];
        String newDateStringPath;
        SimpleDateFormat sdfPath = new SimpleDateFormat(OLD_FORMAT_PATH);
        Date dPath = sdfPath.parse(oldDateStringPath);
        sdfPath.applyPattern(NEW_FORMAT_PATH);
        newDateStringPath = sdfPath.format(dPath);
        System.out.println("Timestamp received in String: " + newDateStringPath);
        String imagePath = "/" + "s_defaultScene_" + newDateStringPath + "_" + split[4] + ".jpg";

        jagIntrusionAlarm.setImgPath(imagePath);
        jagIntrusionAlarm.setVideoPath("");
        jagIntrusionAlarm.setSceneName(split[11]);
        jagIntrusionAlarm.setName(split[4]);
        jagIntrusionAlarm.setContextName("Default");

        long millis = dPath.getTime();
        jagIntrusionAlarm.setGenTimeStamp(millis);
        jagIntrusionAlarm.setTimeStamp(millis);

        String sdkConfId = Long.toString(millis) + "Alarm" + split[2];
        jagIntrusionAlarm.setSdkConfId(sdkConfId);

        if (split[7].equals("1")) {
            jagIntrusionAlarm.setLocation("1.3504738,103.9877264");
        } else if (split[7].equals("3")) {
            jagIntrusionAlarm.setLocation("1.3517292,103.9882527");
        } else {
            jagIntrusionAlarm.setLocation("");
        }

        System.out.print("JSON generated: " + jagIntrusionAlarm.getjsonAlarm().toString());

        /*if (typeAlarm.equalsIgnoreCase("INTRUSION")) {

            try {
                executePost(Utils.INTRUSION, jagIntrusionAlarm.getjsonAlarm());

            } catch (IOException e) {
                e.printStackTrace();
            }

        }*/
    }
}
