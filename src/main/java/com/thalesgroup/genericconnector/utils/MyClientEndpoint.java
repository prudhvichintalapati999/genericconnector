package com.thalesgroup.genericconnector.utils;

import org.slf4j.LoggerFactory;

import javax.websocket.*;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

@ClientEndpoint
public class MyClientEndpoint {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(MyClientEndpoint.class);

	@OnOpen
	public void onOpen(Session session) {
		logger.info("Connected to endpoint: " + session.getBasicRemote());
		try {
			String registration = ConfigurationHelper.getConfigHelperInstance().get3MRegistrationId();
			session.getBasicRemote().sendText(registration);
			logger.info("Sending message to endpoint: " + registration);

		} catch (IOException ex) {
			logger.error(ex.getMessage());
		}
		
	}

	@OnClose
	public void processClose() {
		logger.info("Connection closed");
	}

	@OnMessage
	public void processMessage(String message) {
		try {
			logger.info("Received message in client: " + message);
			if (message.indexOf(ConfigurationHelper.getConfigHelperInstance().get3MMessageIndex()) == 0) {
				M3AlarmHelper.updateAlarmData(M3AlarmHelper.getTransId(message));
			} else {
				logger.info("Message index is non zero");
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} catch (TransformerException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} catch (JAXBException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} catch (InterruptedException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	@OnError
	public void processError(Throwable t) {
		t.printStackTrace();
	}
}