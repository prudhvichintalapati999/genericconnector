package com.thalesgroup.genericconnector.utils;

import com.google.gson.JsonObject;

public class IntrusionAlarm implements SafeAlarmIntrusionInterface{
	JsonObject alarmJson;
	
	public IntrusionAlarm() {
		alarmJson = new JsonObject();
	}
	
	@Override
	public void setStreamId(String streamID) {
		alarmJson.addProperty(SafeAlarmIntrusionInterface.STREAM_ID, streamID);
	}

	@Override
	public void setDuration(int duration) {
		alarmJson.addProperty(SafeAlarmIntrusionInterface.DURATION, duration);
	}

	@Override
	public void setSdkId(String sdkId) {
		alarmJson.addProperty(SafeAlarmIntrusionInterface.SDK_ID, sdkId);
	}

	@Override
	public void setSdkConfId(String sdkConfId) {
		alarmJson.addProperty(SafeAlarmIntrusionInterface.SDKCONF_ID, sdkConfId);
	}

	@Override
	public void setStatus(String status) {
		alarmJson.addProperty(SafeAlarmIntrusionInterface.STATUS, status);
	}

	@Override
	public void setEventId(String eventId) {
		alarmJson.addProperty(SafeAlarmIntrusionInterface.EVENTID, eventId);
	}

	@Override
	public void setImgPath(String imgPath) {
		alarmJson.addProperty(SafeAlarmIntrusionInterface.IMGPATH, imgPath);
	}

	@Override
	public void setVideoPath(String videoPath) {
		alarmJson.addProperty(SafeAlarmIntrusionInterface.VIDEOPATH, videoPath);
	}

	@Override
	public void setSceneName(String sceneName) {
		alarmJson.addProperty(SafeAlarmIntrusionInterface.SCENENAME, sceneName);
	}

	@Override
	public void setName(String name) {
		alarmJson.addProperty(SafeAlarmIntrusionInterface.NAME, name);
	}

	@Override
	public void setContextName(String contextName) {
		alarmJson.addProperty(SafeAlarmIntrusionInterface.CONTEXT_NAME, contextName);
	}

	public void setGenTimeStamp(long genTimeStamp) {
		alarmJson.addProperty(SafeAlarmIntrusionInterface.GENTIMESTAMP, genTimeStamp);
	}

	@Override
	public void setTimeStamp(long timeStamp) {
		alarmJson.addProperty(SafeAlarmIntrusionInterface.TIMESTAMP, timeStamp);
	}

	@Override
	public void setLocation(String location) {
		alarmJson.addProperty(SafeAlarmIntrusionInterface.LOCATION, location);
	}
	
	public JsonObject getjsonAlarm() {
		return alarmJson;
	}
	
}
