package com.thalesgroup.genericconnector.utils;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketMain {
    private int port = 5558;
    public ServerSocket socket;
    public Socket clientSock;

    public SocketMain() {
        init();
    }

    public static void main(String[] args) {
        new SocketMain();
    }

    private void init() {
        try {
            socket = new ServerSocket(port);
            System.out.println("Server started, bound to port: "+port);
            clientSock = socket.accept();
            File directory = new File("./Storage/");
            if (!directory.exists()) {
                directory.mkdirs();
            }
            File file = new File("D:\\genericconnector\\xyz.txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            DataInputStream in = new DataInputStream(clientSock.getInputStream());
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            String line;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
                bw.write(line+"\n");
                bw.flush();
//                bw.close();
            }
            socket.close();
            clientSock.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
