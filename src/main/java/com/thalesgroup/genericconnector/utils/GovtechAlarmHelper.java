package com.thalesgroup.genericconnector.utils;

import com.thalesgroup.genericconnector.alertStructure.Alert;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.net.Socket;

public class GovtechAlarmHelper {

	static Socket socket = null;
	static ObjectOutputStream oos = null;
	ObjectInputStream ois = null;

	// TBD: create an init() function

	// Static InputStream input = null;

	// input = new
	// FileInputStream("C:\\Users\\t0100802\\Desktop\\Hugo\\Eclipse\\3MConnection\\src\\config.properties");

	public static String ipaddSVC = "192.165.10.4";
	public static int portSVC = 9999;

	public static void InItGovtech() {
		try {
			socket = new Socket("192.165.10.4", 9999);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Alert createAlert(Object[] jsonTable) throws IOException {

		Alert alert = new Alert();
		Alert.DetectedItems detectedItems = new Alert.DetectedItems();
		Alert.DetectedItems.DetectedItem detectedItem = new Alert.DetectedItems.DetectedItem();
		Alert.DetectedItems.DetectedItem.Object object = new Alert.DetectedItems.DetectedItem.Object();
		Alert.DetectedItems.DetectedItem.Polygon polygon = new Alert.DetectedItems.DetectedItem.Polygon();

		Alert.DetectedItems.DetectedItem.Polygon.Point point1 = new Alert.DetectedItems.DetectedItem.Polygon.Point();
		Alert.DetectedItems.DetectedItem.Polygon.Point point2 = new Alert.DetectedItems.DetectedItem.Polygon.Point();
		Alert.DetectedItems.DetectedItem.Polygon.Point point3 = new Alert.DetectedItems.DetectedItem.Polygon.Point();
		Alert.DetectedItems.DetectedItem.Polygon.Point point4 = new Alert.DetectedItems.DetectedItem.Polygon.Point();

		String encodedImage = M3AlarmHelper.createImageString();

		alert.setDetectedItems(detectedItems);

		alert.setProcessID((long) 0000);
		alert.setAnalytic("FaceRecognition");
		alert.setFramework("Standalone");
		if (jsonTable[17].equals("WHITE_LIST")) {
			alert.setProvider("3M" + " WL : " + (String) jsonTable[14]);
		}

		else {
			alert.setProvider("3M" + " BL : " + (String) jsonTable[14]);
		}
		// alert.setProvider((String) jsonTable[17] + " : " + (String)
		// jsonTable[14]);
		// alert.setProvider("3M-LFIS");
		alert.setTimestamp(Long.parseLong(String.valueOf(jsonTable[3])));
		alert.setSource("camera-4");
		alert.setImage(encodedImage);

		String fistAppearedTime = (String) jsonTable[3];
		long timestamp = Long.parseLong(fistAppearedTime);
		alert.setTimestamp(timestamp);

		detectedItems.getDetectedItem().add(detectedItem);

		detectedItem.setPolygon(polygon);

		polygon.getPoint().add(0, point1);
		polygon.getPoint().add(1, point2);
		polygon.getPoint().add(2, point3);
		polygon.getPoint().add(3, point4);

		/*
		 * point1.setX((float) 0.0791146); point1.setY((float) 0.602778);
		 * point2.setX((float) 0.872917); point2.setY((float) 0.602778);
		 * point3.setX((float) 0.872917); point3.setY((float) 0.785185);
		 * point4.setX((float) 0.791146); point4.setY((float) 0.785185);
		 */

		point1.setX((float) 0.0);
		point1.setY((float) 0.0);
		point2.setX((float) 1.0);
		point2.setY((float) 0.0);
		point3.setX((float) 1.0);
		point3.setY((float) 1.0);
		point4.setX((float) 0.0);
		point4.setY((float) 1.0);
		// polygon.getPoint().add(0, element);

		detectedItem.setObject(object);
		// object.setObjectID((long) jsonTable[0]);
		// object.setSubjectID((Long) jsonTable[13]);
		object.setName((String) jsonTable[14]);
		object.setScore((float) jsonTable[18]);
		object.setPermissionLists((String) jsonTable[17]);

		return alert;
	}

	public static void createXMLFile(Alert alert) throws JAXBException {

		File file = new File("C:\\Users\\t0100802\\Desktop\\Hugo\\Eclipse\\3MConnection\\src\\LatestXML.xml");
		JAXBContext jaxbContext = JAXBContext.newInstance(Alert.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(alert, file);
		// jaxbMarshaller.marshal(alert, System.out);

	}

	public static void sendNewXML() throws IOException {
		oos = new ObjectOutputStream(socket.getOutputStream());
		System.out.println("Sending request to socket server");
		File file = new File("C:\\Users\\t0100802\\Desktop\\Hugo\\Eclipse\\3MConnection\\src\\LatestXML.xml");
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		StringBuilder sb = new StringBuilder();
		while ((line = br.readLine()) != null) {
			sb.append(line.trim());
		}
		oos.writeObject(sb.toString());
	}

	public static void sendXML() throws IOException, InterruptedException {

		// InetAddress host = InetAddress.getLocalHost();
		Socket socket = null;
		ObjectOutputStream oos = null;
		// ObjectInputStream ois = null;

		socket = new Socket(ipaddSVC, portSVC);

		oos = new ObjectOutputStream(socket.getOutputStream());
		// System.out.println("Sending request to socket server");

		File file = new File("C:\\Users\\t0100802\\Desktop\\Hugo\\Eclipse\\3MConnection\\src\\LatestXML.xml");
		// byte[] content = Files.readAllBytes(file.toPath());
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		StringBuilder sb = new StringBuilder();
		while ((line = br.readLine()) != null) {
			sb.append(line.trim());
		}
		br.close();
		oos.writeObject(sb.toString());
		socket.close();
		oos.close();
		// socket.close();

		Thread.sleep(2000);

	}
}
