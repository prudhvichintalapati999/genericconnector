package com.thalesgroup.genericconnector.utils;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

public class AuthentificationHelper3M {

	
	/*public static String getURL(){
//		String userName = "admin";
//		String uniqueID="LFISWebToken";

//		String url ="http://192.168.19.11/authenticationsrv/user/token?userName=" + userName+"&uniqueID="+uniqueID;
		String url = ConfigurationHelper.getConfigHelperInstance().get3MAuthentificationUri();
		return url;
		
	}*/
	
	public static String getToken() throws IOException {
		
//	String password = "12345";
	
	URL obj = new URL(ConfigurationHelper.getConfigHelperInstance().get3MAuthentificationUri());
	URLConnection conn = obj.openConnection();
	conn.setRequestProperty("password", ConfigurationHelper.getConfigHelperInstance().get3MRequestPropertyPwd());

	//get all headers
	
	//Map<String, List<String>> map = conn.getHeaderFields();
	
	//for (Map.Entry<String, List<String>> entry : map.entrySet()) {
	//	System.out.println("Key : " + entry.getKey() +
	//                 " ,Value : " + entry.getValue());
	//}

	//get header by 'key'
	String userToken = conn.getHeaderField("UserToken");
	//System.out.println(UserToken);
	
	return userToken;
	}
}
