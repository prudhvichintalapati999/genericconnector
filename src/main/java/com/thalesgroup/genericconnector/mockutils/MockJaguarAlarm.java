package com.thalesgroup.genericconnector.mockutils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 * This class is used to Mock alarms as Jaguar sends(For Testing purpose)
 */
public class MockJaguarAlarm {
    private static Socket socket;

    public static void main(String args[]) {
        int counter = 0;
        while (true) {
            try {
                Thread.sleep(1000);
                counter++;
                String host = "localhost";
                int port = 8075;
                InetAddress address = InetAddress.getByName(host);
                socket = new Socket(address, port);
                OutputStream os = socket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os);
                BufferedWriter bw = new BufferedWriter(osw);

                String msg = "0@1@eventid@2017-09-14_10:27:27@Name@5@6@streamId@8@9@10@SceneName@12" + counter;
                String sendMessage = msg + "\n";
                bw.write(sendMessage);
                bw.flush();
                System.out.println("Message sent to the server : " + sendMessage);

            } catch (IOException exception) {
                System.out.println("Server is still offline");/*This should only print once*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
