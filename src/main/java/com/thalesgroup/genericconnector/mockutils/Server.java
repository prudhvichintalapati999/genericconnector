package com.thalesgroup.genericconnector.mockutils;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private static Socket socket;

    public static void main(String[] args) {
        try {
            Thread.sleep(1000);
            int port = 8075;
            ServerSocket serverSocket = new ServerSocket(port);

            //Server is running always. This is done using this while(true) loop
            while (true) {
                //Reading the message from the client
                socket = serverSocket.accept();

//                System.out.println("Client has connected!");
                InputStream is = socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String msg = br.readLine();
                System.out.println("Message received from client is " + msg);

                //Multiplying the number by 2 and forming the return message
                String returnMessage;
                /*try {
//                    int numberInIntFormat = Integer.parseInt(number);
//                    int returnValue = numberInIntFormat * 2;
//                    returnMessage = String.valueOf(returnValue) + "\n";
                } catch (NumberFormatException e) {
                    //Input was not a number. Sending proper message back to client.
                    returnMessage = "Please send a proper number\n";
                }*/

                /*returnMessage = "0@1@eventid@oldDateStringPath@Name@5@6@streamId@8@9@10@SceneName@12";
//                returnMessage = "xyz";
                //Sending the response back to the client.
                OutputStream os = socket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os);
                BufferedWriter bw = new BufferedWriter(osw);
                bw.write(returnMessage);
                System.out.println("Message sent to the client is " + returnMessage);
                bw.flush();*/

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
