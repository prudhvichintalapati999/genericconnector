package com.thalesgroup.genericconnector;

import com.thalesgroup.genericconnector.utils.MyClientEndpoint;
import org.slf4j.LoggerFactory;

import javax.websocket.ContainerProvider;
import javax.websocket.WebSocketContainer;
import java.net.URI;

public class ThreeM {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ThreeM.class);

    public void startAlarmReception(String uri) {
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            logger.info("Connecting to " + uri);
            container.connectToServer(MyClientEndpoint.class, URI.create(uri));
            logger.info("Connected to 3M server");
        } catch (Exception e) {
            logger.error("Error in connecting to 3M server");
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }
}
