package com.thalesgroup.genericconnector;

import com.thalesgroup.genericconnector.utils.ConfigurationHelper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements CommandLineRunner{
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Application.class);

    @Autowired
    private ConfigurationHelper configuration;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
//        ZMQPUSHPULLHelper.startSink();
        //instantiate configuration
        ConfigurationHelper.setConfigurationInstance(configuration);

        startJaguarAlarmReceptionTask();
        start3MAlarmReception();
    }

    private void start3MAlarmReception(String mWebSocketUri) {
        Thread m3AlarmReceptionTask = new Thread(new Runnable() {
            @Override
            public void run() {
                new ThreeM().startAlarmReception(ConfigurationHelper.getConfigHelperInstance().get3MWebSocketUri());
            }
        });

        m3AlarmReceptionTask.start();
    }

    /**
     * Jaguar Alarm Reception Task
     */
    public void startJaguarAlarmReceptionTask() {
        Thread jaguarAlarmReceptionTask = new Thread(new Runnable() {
            @Override
            public void run() {
                new Jaguar().startJaguarAlarmReception(ConfigurationHelper.getConfigHelperInstance().getJagReceptionPort());
            }
        });

        jaguarAlarmReceptionTask.start();
    }
}
